#ifndef MBPORT_INCLUDE_MBREG_H_
#define MBPORT_INCLUDE_MBREG_H_

#include <limits.h>

/* ----------------------- MODBUS Data ------------------------------------------*/

#define REG_COILS_START      (10001)  //coils starting address
#define REG_COILS_NREGS      (0)      //number of coils

#define REG_DISCRETE_START   (20001)  //discretes starting addres
#define REG_DISCRETE_NREGS   (0)      //number of discrete inputs

#define REG_INPUT_START      (30001)  //input registers starting address
#define REG_INPUT_NREGS      (0)      //number of input registers

#define REG_HOLDING_START    (40001)  //holding registers starting address
#define REG_HOLDING_NREGS    (0)      //number of holding registers

/* ----------------------- Other Constants --------------------------------------*/

#define REG_ADDR_START       (1)      //bit/register address offset
#define REG_NUM_BYTES        (2)      //number of bytes in register

/* ----------------------- Global Variables -------------------------------------*/

UCHAR  ucMBCoils[(REG_COILS_NREGS + (CHAR_BIT - 1)) / CHAR_BIT];
UCHAR  ucMBDiscretes[(REG_DISCRETE_NREGS + (CHAR_BIT - 1)) / CHAR_BIT];
USHORT usMBInputReg[REG_INPUT_NREGS];
USHORT usMBHoldingReg[REG_HOLDING_NREGS];

/* ----------------------- Helper Functions -------------------------------------*/

//BOOL bMBCoilIsWritten(UCHAR ucIndex);
//BOOL bMBHoldingRegIsWritten(UCHAR ucIndex);

#endif /* MBPORT_INCLUDE_MBREG_H_ */
