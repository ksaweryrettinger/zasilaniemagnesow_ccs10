#include <rw_tables.h>

static volatile BOOL bI2CTimeout = FALSE;

/* ----------------------- Function definitions -----------------------------------*/

void xWriteAddress(USHORT usAddress)
{
    UCHAR ucA8, ucA9;
    static BOOL bInitAddress = TRUE;
    static USHORT usTableAddress = 0;

    if ((usAddress != usTableAddress) || bInitAddress) //new table address
    {
        if (bInitAddress) bInitAddress = FALSE;
        usTableAddress = usAddress; //store latest address

        SSIDataPut(SSI0_BASE, (UCHAR)(usAddress & 0xFF)); //write address bits A0-A7

        //Write address bits A8-A9
        ucA8 = (usAddress >> 8) & 1;
        ucA9 = (usAddress >> 9) & 1;
        GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_7, ucA8 << 7);
        GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_5, ucA9 << 5);

        while (SSIBusy(SSI0_BASE)) {} //wait for SPI transaction to complete

        //PE1-PE3 = 010, 111
        GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, GPIO_PIN_2);
        GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);
    }
}

void xWrite8BitTable(UCHAR ucData)
{
    //PB5 = 0
    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_5, 0);

    //Write data bits D0-D7
    GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, ucData & 0x0F); //D0-D3
    GPIOPinWrite(GPIO_PORTC_BASE, GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7, ucData & 0xF0); //D4-D7

    //PE1-PE3 = 000, 111
    GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, 0);
    GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);

    //PE0 = PB0 = 0
    GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_0, 0);
    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_0, 0);

    //180ns delay
    SysCtlDelay(SYS_DELAY_WRITE);

    //PB0, PE0 = 1
    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_0, GPIO_PIN_0);
    GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_0, GPIO_PIN_0);

    //PB5 = 1
    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_5, GPIO_PIN_5);
}

UCHAR xRead8BitTable(void)
{
    ULONG lTableData;

    //PB5 = 0
    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_5, 0);

    //PE1-PE3 = 110, PB1 = 0
    GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, GPIO_PIN_1 | GPIO_PIN_2);
    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_1, 0);

    //120ns delay
    SysCtlDelay(SYS_DELAY_READ);

    //Read data bits D0-D7 and store in Modbus register
    lTableData = GPIOPinRead(GPIO_PORTD_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);
    lTableData |= GPIOPinRead(GPIO_PORTC_BASE, GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7);

    //PE1-PE3 = 111
    GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);

    //PB1 = 1, PB5 = 1
    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_1, GPIO_PIN_1);
    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_5, GPIO_PIN_5);

    return (UCHAR)(lTableData & 0xFF);
}

void xWrite16BitTable(USHORT usData)
{
    //PB5 = 0
    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_5, 0);

    //Write data bits D0-D7
    GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, usData & 0x000F); //D0-D3
    GPIOPinWrite(GPIO_PORTC_BASE, GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7, usData & 0x00F0); //D4-D7

    //PE1-PE3 = 000, 111
    GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, 0);
    GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);

    //Write data bits D8-D15
    GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, (usData & 0x0F00) >> 8); //D8-D11
    GPIOPinWrite(GPIO_PORTC_BASE, GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7, (usData & 0xF000) >> 8); //D12-D15

    //PE1-PE3 = 101, 111
    GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, GPIO_PIN_1 | GPIO_PIN_3);
    GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);

    //PE0 = PB0 = 0
    GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_0, 0);
    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_0, 0);

    //180ns delay
    SysCtlDelay(SYS_DELAY_WRITE);

    //PB0 = 1, PE0 = 1, PB5 = 1
    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_0, GPIO_PIN_0);
    GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_0, GPIO_PIN_0);
    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_5, GPIO_PIN_5);
}

USHORT xRead16BitTable(void)
{
    ULONG lTableData;

    //PB5 = 0
    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_5, 0);

    //PE1-PE3 = 110, PB1 = 0
    GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, GPIO_PIN_1 | GPIO_PIN_2);
    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_1, 0);

    //120ns delay
    SysCtlDelay(SYS_DELAY_READ);

    //Read data bits D0-D7
    lTableData = GPIOPinRead(GPIO_PORTD_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);
    lTableData |= GPIOPinRead(GPIO_PORTC_BASE, GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7);

    //PE1-PE3 = 011
    GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, GPIO_PIN_2 | GPIO_PIN_3);

    //Read data bits D8-D15 and store in Modbus register
    lTableData |= (GPIOPinRead(GPIO_PORTD_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3) << 8);
    lTableData |= (GPIOPinRead(GPIO_PORTC_BASE, GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7) << 8);

    //PE1-PE3 = 111
    GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);

    //PB1 = 1, PB5 = 1
    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_1, GPIO_PIN_1);
    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_5, GPIO_PIN_5);

    return (USHORT)(lTableData & 0xFFFF);
}

void xWriteI2C(UCHAR ucData)
{
    ULONG ulErrorI2C = I2C_MASTER_ERR_NONE;

    //I2C send mode
    I2CMasterSlaveAddrSet(I2C0_BASE, ADDRESS_PCF8574, FALSE);

    //Send data byte
    I2CMasterDataPut(I2C0_BASE, ucData);
    I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_SINGLE_SEND);
    xI2CMasterWait(&ulErrorI2C);
}

UCHAR xReadI2C(void)
{
    ULONG ulErrorI2C = I2C_MASTER_ERR_NONE;
    ULONG ulData = 0;

    //I2C Receive Mode
    I2CMasterSlaveAddrSet(I2C0_BASE, ADDRESS_PCF8574, TRUE);
    I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_SINGLE_RECEIVE);
    xI2CMasterWait(&ulErrorI2C);

    //Read data byte
    if (ulErrorI2C == I2C_MASTER_ERR_NONE) ulData = I2CMasterDataGet(I2C0_BASE);
    return (UCHAR) ulData;
}

void xI2CMasterWait(ULONG * pulErrorI2C)
{
    //Start I2C timeout timer
    bI2CTimeout = FALSE;
    TimerLoadSet(TIMER1_BASE, TIMER_BOTH, I2C_MASTER_TIMEOUT);
    TimerEnable(TIMER1_BASE, TIMER_BOTH);

    //Wait for response
    while (I2CMasterBusy(I2C0_BASE))
    {
        if (bI2CTimeout) //check for timeout
        {
            //Timeout occured
            *pulErrorI2C = I2C_MASTER_ERR_CLK_TOUT;
            return;
        }
    }

    TimerDisable(TIMER1_BASE, TIMER_BOTH); //response received, switch off timer

    //Check for other errors
    *pulErrorI2C = I2CMasterErr(I2C0_BASE);
    return;
}

void Timer1IntHandler(void)
{
    uint32_t ui32status;
    ui32status = TimerIntStatus(TIMER1_BASE, TRUE);
    TimerIntClear(TIMER1_BASE, ui32status);
    bI2CTimeout = TRUE;
}
